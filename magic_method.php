<?php
/**
 * Magic Method
 * Date: 10/26/2016
 * Time: 12:29 PM
 */

class ImAClass{

    public function __construct($value){
        echo $value;
    }

    public function __destruct(){
        echo "GoodBye!";
    }

    public function __call($name,$arguments){
            echo $name."<br>";
            print_r($arguments);
    }


    public function shows(){
        echo "Function exists!";
    }


    public static $msg;
    public static function message()
    {
        echo self::$msg;
    }

    public static function __callStatic($name, $arguments)
    {
        echo $name;
        echo $arguments;
    }

}

/*
    $obj= new ImAClass("Hello Class!");

    $obj->show(4,0,0);
*/
    ImAClass::$msg="Deleted!";
    ImAClass::message();
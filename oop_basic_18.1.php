<?php
/* OOP Basic 2
 * Session 18
 * Date: 10/26/2016
 * Time: 9:49 AM
 */

    class BITM{

        public $window;
        public $door;
        public $chair;
        public $table;
        public $whiteboard;


        public function airCooling(){
            echo "I am the Air Cooler";
        }

        public function compute(){
            echo "I am computing";
        }

        public function show(){
            echo $this->window."<br>";
            echo $this->door."<br>";
            echo $this->table."<br>";
            echo $this->chair."<br>";
            echo $this->whiteboard."<br>";

        }

        public function setWindow($win){
            $this->window=$win;
        }

        public function setDoor($door)
        {
            $this->door=$door;
        }

        public function setTable($tab)
        {
            $this->table = $tab;
        }

        public function setWhiteboard($board)
        {
            $this->whiteboard = $board;
        }

        public function setChair($chair)
        {
            $this->chair = $chair."[ set by Parent]";
        }



    } //End of Parent Class


class BITM_Lab402 extends BITM{

    public function setChair($chair)
    {
        $chair= $chair. "[ set by child ]";
        parent::setChair($chair);

    }


}


$obj_BITM_at_CTG = new BITM;



$obj_BITM_at_CTG->setChair("Regal Chair!");
$obj_BITM_at_CTG->setTable("Board Table!");
$obj_BITM_at_CTG->setWindow("Thai Glass!");
$obj_BITM_at_CTG->setDoor("White Board!!");
$obj_BITM_at_CTG->setWhiteboard("Glass door!");

$obj_BITM_at_CTG->show();



$obj_new_lab=new BITM_Lab402();
$obj_new_lab->setChair("RFL Chair!");
$obj_new_lab->setTable("Glass Table!");
$obj_new_lab->setWindow("Wooden!");
$obj_new_lab->setDoor("No Board!!");
$obj_new_lab->setWhiteboard("Wooden Door!");

$obj_new_lab->setChair("Ami ekti chair");
$obj_new_lab->show();

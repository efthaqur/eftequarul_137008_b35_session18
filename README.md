OOP Basic -2
===================


Date 26 Oct 2016
----------


Session 18
-------------

**OOP Basic 2**

> **Note:**

> - **Review Class** 
>- Coding Example on inheritance.

> -  Review Previous Class

> - Scope resolution operator in PHP

> - parent-child.php by Sir

> - **Session 18 topics**  
>-  Encapsulation-Public Lesson

> -  Encapsulation-Protected Lesson

> - Encapsulation-Private Lesson

> -  Magic Method Lesson



